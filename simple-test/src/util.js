//Here inside our util.js, we're going to right features and aspects that we want to implement a unit test


/*
	Goal is to create a unit test for our feature called Login

	Login -
		- Form Validation
			- email -> test suites
				- check if it is empty
				- check if it is undefined
				- check if it is null
				- check if it is string
*/

function checkEmail(email){
	//To resolve the failed test from our unit testing, we will create a solution
	if(email == "") {
		return "Error: Email is Empty!";
	}
  
	if(email === undefined){
		return "Error: Email should be defined!";
	}

	if(email === null) {
		return "Error: Email should not be null!";
	}

	if(typeof(email) !== 'string'){
		return "Error: Email should be a string value!";
	}

	return email;
}


function checkAge(age) { //numeric/integer
	if(age == "") {
		return "InputError: Age must have a value!";
	}

	if(typeof age != 'number'){
		return "InputError: Age must be a number!";
	}

	if(age === undefined) {
		return "InputError: Age must be defined!";
	}

	if(age < 0) {
		return "InputError: Age must be greater than 0";
	}

	return age; //six
}


function checkFullName(fullName){
	return fullName;
}

module.exports = {
	checkEmail: checkEmail,
	checkAge: checkAge,
	checkFullName: checkFullName
}
